﻿using ExemplosConexoes;
using ExemplosConsulta.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;

namespace ExemplosConsulta
{
    public class ExemploSacado
    {
        #region Listar()
        public static List<ModeloSacado> Listar()
        {
            string cadeiaDeConexao = "HOTS=exemplo.ddns.com;PORT=4040;USER=user;PASSWORD=123456";

            using (ConexaoSGI conexaoSGI = new ConexaoSGI(cadeiaDeConexao))
            {
                List<DbParameter> parametros = new List<DbParameter>();

                #region script
                string script = @"
                SELECT  codigo,
                        bandeira
                FROM    recebimentos
                ";
                #endregion

                List<ModeloSacado> sacados;

                // Consulta como lista
                sacados = conexaoSGI.Consultar<ModeloSacado>(script);

                // Consulta como tabela, e converte para lista
                DataTable tabela = conexaoSGI.Consultar(script);

                sacados = tabela
                    .AsEnumerable()
                    .Select(row => new ModeloSacado
                    {
                        codigo = Convert.ToInt32(row["codigo"]),
                        bandeira = Convert.ToString(row["bandeira"])
                    })
                    .ToList();

                // Consulta com parametros
                sacados = conexaoSGI.Consultar<ModeloSacado>(script, parametros);

                // Consulta com timeout
                sacados = conexaoSGI.Consultar<ModeloSacado>(script, tempo: 120);

                // Consulta com parametros e timeout
                sacados = conexaoSGI.Consultar<ModeloSacado>(script, parametros, 120);

                return sacados;
            }
        }
        #endregion

        #region Contar()
        public static int ContarSacados()
        {
            string cadeiaDeConexao = "HOTS=exemplo.ddns.com;PORT=4040;USER=user;PASSWORD=123456";

            using (ConexaoAutoSystem conexaoSGI = new ConexaoAutoSystem(cadeiaDeConexao))
            {
                #region script
                string script = @"
                SELECT  COUNT(*)
                FROM    recebimentos
                ";
                #endregion

                // Consulta como tipo primitivo
                int contagem = conexaoSGI.Consultar<int>(script).First();

                return contagem;
            }
        }
        #endregion

        #region Remover(codigo)
        public static bool Remover(int codigo)
        {
            string cadeiaDeConexao = "HOTS=exemplo.ddns.com;PORT=4040;USER=user;PASSWORD=123456";

            using (ConexaoApollo conexaoSGI = new ConexaoApollo(cadeiaDeConexao))
            using (DbTransaction transacao = conexaoSGI.IniciarTransacao())
            {
                #region script
                string script = @"
                REMOVE
                FROM    recebimentos
                WHERE   codigo = @codigo
                ";
                #endregion

                List<DbParameter> parametros = new List<DbParameter>();

                /*  Uso de parametros 1 - Preencher a lista com a biblioteca do provedor sql*/
                parametros.Add(new SqlParameter("@codigo", codigo));

                /*  Uso de parametros 2 - Tornar o atrubuto FabricaSQL publico*/
                //DbParameter paramCodigo = conexaoSGI.FabricaSQL.CreateParameter();
                //paramCodigo.ParameterName = "@codigo";
                //paramCodigo.Value = codigo;
                //parametros.Add(paramCodigo);

                /*  Uso de parametros 3 - Gerar um metodo na classe de conexão para criar os parametros*/
                //parametros.Add(ConexaoSGI.Parametrizar("@codigo", codigo));

                // Executa o comando Delete, e retorna o número de linhas deletadas
                int linhas = conexaoSGI.Executar(script, parametros, transacao);

                if (linhas == 1)
                {
                    transacao.Commit();
                    return true;
                }
                else
                    return false;
            }
        }
        #endregion
    }
}
