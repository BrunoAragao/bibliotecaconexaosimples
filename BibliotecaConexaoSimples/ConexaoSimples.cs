﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;

namespace BibliotecaConexaoSimples
{
    /// <summary>
    /// Simplifica a conexão e as consultas via script ao banco de dados
    /// </summary>
    public class ConexaoSimples : IConexaoSimples, IDisposable
    {
        /// <summary>Conexão com o banco de dados</summary>
        protected readonly DbConnection Conexao;
        /// <summary>Fábrica de recursos do provedor SQL</summary>
        protected readonly DbProviderFactory FabricaSQL;

        /// <summary>
        /// Construtor da classe, configura e abre a conexão
        /// </summary>
        /// <param name="cadeiraDeConexao">String de conexão com o banco de dados</param>
        /// <param name="provedorSQL">Namespace do provider SQL</param>
        public ConexaoSimples(string cadeiraDeConexao, string provedorSQL)
        {
            FabricaSQL = DbProviderFactories.GetFactory(provedorSQL);

            Conexao = FabricaSQL.CreateConnection();
            Conexao.ConnectionString = TratarStringDeConexao(cadeiraDeConexao);
            TratarAberturaDeConexao();
        }

        /// <summary>
        /// Executa um comando SQL e retorna o número de linhas afetadas
        /// </summary>
        /// <param name="script">Script sql</param>
        /// <param name="parametros">Parâmetros do script</param>
        /// <param name="transacao">Transação</param>
        /// <param name="tempo">Tempo limite da consulta, em segundos</param>
        /// <returns>Número de linhas afetadas</returns>
        public int Executar(string script, IEnumerable<DbParameter> parametros = null, DbTransaction transacao = null, int tempo = 60)
        {
            parametros = parametros ?? new DbParameter[0];

            using (DbCommand comando = FabricaSQL.CreateCommand())
            {
                comando.Connection = Conexao;
                comando.Transaction = transacao;
                comando.CommandText = script;
                comando.CommandTimeout = tempo;
                comando.Parameters.AddRange(parametros.ToArray());

                int linhas = comando.ExecuteNonQuery();
                return linhas;
            }
        }

        /// <summary>
        /// Realiza uma consulta SQL e retorna o resultado em uma tabela
        /// </summary>
        /// <param name="script">Script sql</param>
        /// <param name="parametros">Parâmetros do script</param>
        /// <param name="tempo">Tempo limite da consulta, em segundos</param>
        /// <returns>Tabela com resultado da consulta</returns>
        public DataTable Consultar(string script, IEnumerable<DbParameter> parametros = null, int tempo = 60)
        {
            parametros = parametros ?? new DbParameter[0];

            using (DbCommand comando = FabricaSQL.CreateCommand())
            {
                comando.Connection = Conexao;
                comando.CommandText = script;
                comando.CommandTimeout = Conexao.ConnectionTimeout;
                comando.Parameters.AddRange(parametros.ToArray());

                using (DbDataAdapter adaptador = FabricaSQL.CreateDataAdapter())
                {
                    DataTable tabela = new DataTable();
                    adaptador.SelectCommand = comando;
                    adaptador.Fill(tabela);
                    return tabela;
                }
            }
        }

        /// <summary>
        /// Realiza uma consulta SQL e retorna o resultado em uma lista
        /// </summary>
        /// <typeparam name="T">Tipo de elemento na lista</typeparam>
        /// <param name="script">Script sql</param>
        /// <param name="parametros">Parâmetros do script</param>
        /// <param name="tempo">Tempo limite da consulta, em segundos</param>
        /// <returns>Lista com resultado da consulta</returns>
        public List<T> Consultar<T>(string script, IEnumerable<DbParameter> parametros = null, int tempo = 60)
        {
            DataTable tabela = Consultar(script, parametros, tempo);
            return ConexaoSimplesUtils.ConverterParaLista<T>(tabela);
        }

        /// <summary>
        /// Encerra a conexão
        /// </summary>
        public void Dispose()
        {
            Conexao.Dispose();
        }

        /// <summary>
        /// Inicia uma nova transação
        /// </summary>
        /// <returns>Nova transação ligada a conexão</returns>
        public DbTransaction IniciarTransacao()
        {
            return Conexao.BeginTransaction();
        }

        /// <summary>
        /// Trata a abertura de conexão
        /// </summary>
        protected virtual void TratarAberturaDeConexao()
        {
            Conexao.Open();
        }

        /// <summary>
        /// Trata a string de conexão
        /// </summary>
        /// <param name="stringDeConexao">String de conexão não tratada</param>
        /// <returns>String de conexão tratada</returns>
        protected virtual string TratarStringDeConexao(string stringDeConexao)
        {
            return stringDeConexao;
        }
    }
}
