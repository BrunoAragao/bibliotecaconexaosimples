﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace BibliotecaConexaoSimples
{
    public interface IConexaoSimples
    {
        /// <summary>
        /// Executa um comando SQL e retorna o número de linhas afetadas
        /// </summary>
        /// <param name="script">Script sql</param>
        /// <param name="parametros">Parâmetros do script</param>
        /// <param name="transacao">Transação</param>
        /// <param name="tempo">Tempo limite da consulta, em segundos</param>
        /// <returns>Número de linhas afetadas</returns>
        int Executar(string script, IEnumerable<DbParameter> parameters, DbTransaction transaction, int timeout);

        /// <summary>
        /// Realiza uma consulta SQL e retorna o resultado em uma tabela
        /// </summary>
        /// <param name="script">Script sql</param>
        /// <param name="parametros">Parâmetros do script</param>
        /// <param name="tempo">Tempo limite da consulta, em segundos</param>
        /// <returns>Tabela com resultado da consulta</returns>
        DataTable Consultar(string script, IEnumerable<DbParameter> parameters, int timeout);

        /// <summary>
        /// Realiza uma consulta SQL e retorna o resultado em uma lista
        /// </summary>
        /// <typeparam name="T">Tipo de elemento na lista</typeparam>
        /// <param name="script">Script sql</param>
        /// <param name="parametros">Parâmetros do script</param>
        /// <param name="tempo">Tempo limite da consulta, em segundos</param>
        /// <returns>Lista com resultado da consulta</returns>
        List<T> Consultar<T>(string script, IEnumerable<DbParameter> parameters, int timeout);
    }
}
