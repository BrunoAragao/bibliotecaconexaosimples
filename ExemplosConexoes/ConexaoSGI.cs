﻿using BibliotecaConexaoSimples;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ExemplosConexoes
{
    public class ConexaoSGI : ConexaoSimples
    {
        private const string NOME_DO_PROVEDOR_SQL = "Oracle.ManagedDataAccess.Client";

        public ConexaoSGI(string cadeiaDeConexao)
            : base(cadeiaDeConexao, NOME_DO_PROVEDOR_SQL) { }

        protected override void TratarAberturaDeConexao()
        {
            try
            {
                Conexao.Open();
            }
            catch (Exception)
            {
                throw new Exception("Falha de comunicação com o servidor do Cliente");
            }
        }

        protected override string TratarStringDeConexao(string connectionString)
        {
            List<string> parameters = connectionString.Split(';').ToList();
            parameters.RemoveAll(p => p.StartsWith("NUM_CX"));
            parameters.RemoveAll(p => p.StartsWith("COD_USUA"));

            connectionString = String.Join(";", parameters);
            return connectionString;
        }
    }
}
