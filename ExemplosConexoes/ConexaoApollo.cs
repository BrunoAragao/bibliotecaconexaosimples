﻿using BibliotecaConexaoSimples;
using System;

namespace ExemplosConexoes
{
    public class ConexaoApollo : ConexaoSimples
    {
        private const string NOME_DO_PROVEDOR_SQL = "System.Data.SqlClient";

        public ConexaoApollo(string cadeiaDeConexao)
            : base(cadeiaDeConexao, NOME_DO_PROVEDOR_SQL) { }

        protected override void TratarAberturaDeConexao()
        {
            try
            {
                Conexao.Open();
            }
            catch (Exception)
            {
                throw new Exception("Falha de comunicação com o servidor do Cliente");
            }
        }
    }
}
