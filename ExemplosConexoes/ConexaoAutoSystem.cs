﻿using BibliotecaConexaoSimples;
using System;

namespace ExemplosConexoes
{
    public class ConexaoAutoSystem : ConexaoSimples
    {
        private const string NOME_DO_PROVEDOR_SQL = "Npgsql";

        public ConexaoAutoSystem(string cadeiaDeConexao)
            : base(cadeiaDeConexao, NOME_DO_PROVEDOR_SQL) { }

        protected override void TratarAberturaDeConexao()
        {
            try
            {
                Conexao.Open();
            }
            catch (Exception)
            {
                throw new Exception("Falha de comunicação com o servidor do Cliente");
            }
        }

        protected override string TratarStringDeConexao(string connectionString)
        {
            connectionString += ";Encoding=windows-1252;Client Encoding=sql-ascii";
            return connectionString;
        }
    }
}
